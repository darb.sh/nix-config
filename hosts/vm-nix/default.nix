{ config, pkgs, ... }:
# zfs test vm settings
{
imports = [];

i18n = {
consoleFont = "Lat2-Terminus16";
consoleKeyMap = "us";
defaultLocale = "en_US.UTF-8";
};

time.timeZone = "America/Los_Angeles";

programs.mtr.enable = true;
programs.bash.enableCompletion = true;

networking.hostName = "vm-nix";

environment.systemPackages = with pkgs; [
    # system
    gcc
    gnumake
    usbutils

    # virtualization
    open-vm-tools

    # editor
    vim

    # development
    ansible2
    git
    kubernetes
    stack
    terraform
];

  services = {

    vmwareGuest.enable = true;

    # openssh.enable = true;

    xserver = {
      enable = true;
      layout = "us";
      windowManager.xmonad.enable = true;
      windowManager.default = "xmonad";
      windowManager.xmonad.enableContribAndExtras = true;
      desktopManager.xterm.enable = false;
      desktopManager.default = "none";
      displayManager = {
        auto = {
          enable = true;
          user = "darb";
        };
      };
    };
  };
  
  fonts = {
    enableFontDir = true;
    enableGhostscriptFonts = true;
    fonts = with pkgs; [
      hack-font
      source-code-pro
      unifont
    ];
  };


  virtualisation = {
    docker.enable = true;
  };

  users.extraUsers.darb = {
    isNormalUser = true;
    extraGroups = ["wheel" "input" "audio" "video" "docker"];
    uid = 1000;
  };


# system.stateVersion (set to the same version as the ISO installer)

# This value determines the NixOS release with which your system is to be
# compatible, in order to avoid breaking some software such as database
# servers. You should change this only after NixOS release notes say you
# should.
system.stateVersion = "18.09"; # Did you read the comment?
}
